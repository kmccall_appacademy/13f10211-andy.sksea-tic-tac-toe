require_relative 'board'

class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark
  
  def initialize(name)
    @name = name
  end
  
  def display(board)
    @board = board
  end
  
  def get_move
    empty = []
    [*0..2].each do |i|
      coords = win_coords_row(i) || win_coords_col(i)
      return coords if !coords.nil?
      empty.concat(get_empty_coords(i))
    end
    # Random coordinate if no winning move.
    empty[Random.new.rand(0...empty.length)]
  end
  
  private
  
  # Return coordinates if row is winning, or nil.
  def win_coords_row(i)
    row = @board.grid[i]
    row.count { |sym| sym == @mark } == 2 ? [i, row.index(nil)] : nil
  end
  
  # Return coordinates if col is winning, or nil.
  def win_coords_col(i)
    col = @board.grid.reduce([]) { |acc, row| acc << row[i] }
    col.count { |sym| sym == @mark } == 2 ? [col.index(nil), i] : nil
  end
  
  # Return coordinates if left diag is winning, or nil.
  def win_coords_left_diag
    n = 0
    diag = @board.grid.reduce([]) do |acc, row| 
      acc << row[n]
      n += 1
      acc
    end
    diag.count { |sym| sym == @mark } == 2 ? [diag.index(nil), diag.index(nil)] : nil
  end
  
  # Return coordinates if right diag is winning, or nil.
  def win_coords_right_diag
    n = 2
    diag = @board.grid.reduce([]) do |acc, row| 
      acc << row[n]
      n -= 1
      acc
    end
    diag.count { |sym| sym == @mark } == 2 ? [diag.index(nil), 2 - diag.index(nil)] : nil
    # The index of nil here represents the row of winning coord.
    # Coords are in the form [row, col], so calculate col by 2 - index. Visually, the row looks like...
    # - - O
    # - O -
    # O - -
  end
  
  # Returns empty coords for row.
  def get_empty_coords(ri)
    empty = []
    @board.grid[ri].each.with_index { |c, i| empty << [ri, i] if c.nil? }
    empty
  end
  
end
