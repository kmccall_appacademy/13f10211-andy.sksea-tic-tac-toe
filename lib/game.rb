require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :player_one, :player_two, :board, :current_player
  
  def initialize(p1, p2)
    @current_player = p1
    @player_one = p1
    @player_two = p2
    @board = Board.new
  end
  
  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one    
    end
  end
  
  def play_turn
    @current_player.display(board)
    pos = @current_player.get_move
    @board.place_mark(pos, @current_player.mark)
    switch_players!
  end
  
end
