require_relative 'board'

class HumanPlayer
  attr_reader :name
  attr_accessor :mark
  
  def initialize(name)
    @name = name
  end
  
  # Are specs repeated for get_move?
  
  def get_move!
    puts "where?"
    coords = gets.chomp
    coords = coords.split(", ").map { |s| s.to_i }
  end
  
  def get_move
    puts "where?"
    coords = gets.chomp
    coords = coords.split(", ").map { |s| s.to_i }
  end
  
  def display(board)
    visual_board = []
    board.grid.each do |row|
      visual_board << row.map { |sym| sym.nil? ? "_" : sym.to_s }
    end
    visual_board.each { |row| puts row.join }
  end
  
end
