class Board
  attr_reader :grid, :winner
  
  def initialize(grid=nil)
    @n = 3
    @moves = 0
    @grid = grid || []
    @n.times { @grid << Array.new(@n) } if @grid.empty?
  end

  def place_mark(pos, mark)
    if empty?(pos)
      @grid[pos[0]][pos[1]] ||= mark
      @moves += 1
      @winner = mark if won?(pos, mark)
    end
  end
  
  def empty?(pos)
    @grid[pos[0]][pos[1]].nil?
  end
  
  def over?
    !@winner.nil? || (board_full? && @winner.nil?)
  end
  
  private
  
  def board_full?
    @moves == @n ** 2
  end
  
  def won?(pos, sym)
    y, x = pos[0], pos[1]
    won = col_won?(x,sym) || row_won?(y,sym)
    if x + y != 1
      (won = won || left_diag_won?(sym)) if x == y
      (won = won || right_diag_won?(sym)) if x != y
    end
    won
  end
  
  def row_won?(i, sym)
    @grid[i].all? { |c| c == sym } 
  end
  
  def col_won?(i, sym)
    [*0...@n].all? { |x| @grid[x][i] == sym }
  end
  
  def left_diag_won?(sym)
    [*0...@n].all? { |x| @grid[x][x] == sym }
  end
  
  def right_diag_won?(sym)
    [*0...@n].all? { |x| @grid[x][@n-x-1] == sym }
  end
  
end
